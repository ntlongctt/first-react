import React from 'react';

const UserInfo = (props) => {
  return (
    <div>
      <div>
        <div>Name: {props.name}</div>
      </div>
      <div>
        <div>Address: {props.address}</div>
      </div>
      <div>
        <div>Admin: {props.isAdmin}</div>
      </div>
    </div>
  );
}

export default UserInfo;