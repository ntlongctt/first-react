import React, { Component } from 'react';
import UserInfo from '../../components/user-info';
import { connect } from 'react-redux';
import './home.style.css';
import { updateUser } from '../../actions/user.action';

class Home extends Component {

  constructor(props) {
    super(props);

    this.onUpdateUser = this.onUpdateUser.bind(this);
  }

  onUpdateUser() {
    this.props.onUpdateUser('Nguyen Long');
  }

  render() {
    console.log(this.props);
    return (
      <div className="my-body">
        <h1> Wellcome to react!!! </h1>
        <UserInfo name="Long Nguyen" address="HCM" isAdmin="true"/>
        <br/>
        {this.props.user}
        <br/>
        <button onClick={this.onUpdateUser}>up dateUser</ button>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: state.products,
  user: state.user
})

const mapActionsToProps = {
  onUpdateUser: updateUser
}

export default connect(mapStateToProps, mapActionsToProps) (Home);

