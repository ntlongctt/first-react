import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class UserDetail extends Component {
  render() {
    return (
      <div>
        <div>Name {this.props.name} </div>
        <div>Address {this.props.address}</div>
      </div>
    );
  }
}

export default UserDetail;