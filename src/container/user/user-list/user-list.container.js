import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import UserDetail from '../user-detail/user-detail.container';

class UserList extends Component {
  render() {
    return (
      <div>
        <h1> User List </h1>
        {/* <Button variant="raised" color="primary">
          Go Detail
        </Button> */}
        <UserDetail name="Long" address="hcm" />
      </div>
    );
  }
}

export default UserList;