import React, { Component } from 'react';
import './App.css';
import Home from './home/home.container';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import User from './user/user.container';
import UserList from './user/user-list/user-list.container';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/user">User</Link>
            </li>
          </ul>

          <hr />

          <Route exact path="/" component={Home} />
          <Route exact path="/user" component={User} />
          
        <Route
          exact
          path="/user/list"
          component={UserList}
        />
        </div>
      </Router>
    );
  }
} 

export default App;
