import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './container/App';
import { Provider } from 'react-redux';
import { createStore, combineReducers } from 'redux';
import productsReducer from './reducers/products.reducer';
import userReducer from './reducers/user.reducer';

const allReducers = combineReducers({
  products: productsReducer,
  user: userReducer
})

const myAppstore = createStore(
  allReducers, 
  {
    products: [{name: 'SamSung'}],
    user: 'Long'
  },
  window.devToolsExtension && window.devToolsExtension()
);


ReactDOM.render(
  <Provider store={myAppstore}> 
    <App />
  </Provider>, 
  document.getElementById('root')
);